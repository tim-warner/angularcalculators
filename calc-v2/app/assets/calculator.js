/* JavaScript Document */
var current_rates;
var excess_factor;
(function(){
    var app = angular.module('myCalculator', []);


    app.controller('CalculatorController', ['$scope', '$http', function($scope, $http) {

        $scope.rates = $http.get('assets/jsondata/rates.json').success(function (data) {
            current_rates = data.rates;
            excess_factor = data.excessfactor;
            $scope.calculate();
            console.log(data.rates);
            console.log(data.excessfactor)
        });

        $scope.current_salary = 26000;
        $scope.current_age = 40;
        $scope.excess_options = [0,100,250,500,1000];
        $scope.current_excess = 100;
        $scope.single_value = 1100;

        $scope.calculate = function(){

            if ($scope.current_age < 16){$scope.current_age = 16}

            if ( $scope.current_age < 20){$scope.rate_array = 0 }
            if ( $scope.current_age < 25 && $scope.current_age >= 20) { $scope.rate_array = 1 }
            if ( $scope.current_age < 30 && $scope.current_age >= 25) { $scope.rate_array = 2 }
            if ( $scope.current_age < 35 && $scope.current_age >= 30) { $scope.rate_array = 3 }
            if ( $scope.current_age < 40 && $scope.current_age >= 35) { $scope.rate_array = 4 }
            if ( $scope.current_age < 45 && $scope.current_age >= 40) { $scope.rate_array = 5 }
            if ( $scope.current_age < 50 && $scope.current_age >= 45) { $scope.rate_array = 6 }
            if ( $scope.current_age < 55 && $scope.current_age >= 50) { $scope.rate_array = 7 }
            if ( $scope.current_age < 60 && $scope.current_age >= 55) { $scope.rate_array = 8 }
            if ( $scope.current_age < 65 && $scope.current_age >= 60) { $scope.rate_array = 9 }
            if ( $scope.current_age >= 65) { $scope.rate_array = 10 }

            $scope.rate = current_rates[$scope.rate_array];

            if ( $scope.current_excess === 0){$scope.excess_array = 0 }
            if ( $scope.current_excess === 100) { $scope.excess_array = 1 }
            if ( $scope.current_excess === 250) { $scope.excess_array = 2 }
            if ( $scope.current_excess === 500) { $scope.excess_array = 3 }
            if ( $scope.current_excess === 1000) { $scope.excess_array = 4 }


            $scope.excess_factor = excess_factor[$scope.excess_array];

            $scope.single_cost = (($scope.rate) * $scope.excess_factor).toFixed(2);
            $scope.couple_cost = (($scope.rate * 2) * $scope.excess_factor).toFixed(2);
            $scope.parent_cost = (($scope.rate * 1.5) * $scope.excess_factor).toFixed(2);
            $scope.family_cost = (($scope.rate * 2.5) * $scope.excess_factor).toFixed(2);

            if ($scope.current_salary <= 31785 ){$scope.tax = .88}
            if ($scope.current_salary > 31785 ){$scope.tax = .98}

            $scope.single_net_cost = (($scope.single_cost * $scope.tax)/12).toFixed(2);
            $scope.couple_net_cost = (($scope.couple_cost * $scope.tax)/12).toFixed(2);
            $scope.parent_net_cost = (($scope.parent_cost * $scope.tax)/12).toFixed(2);
            $scope.family_net_cost = (($scope.family_cost * $scope.tax)/12).toFixed(2);


        }



    }])
})();


